
package imageClasses;

import java.awt.Image;
import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class ImageCreator {
    protected String imageAddr; //путь к изображению
    protected  Image img;//обьект "изображение"
    
    
     protected int x;

   
    
     public Image getImage() {
         if(img ==null){
             createImage();
         }
        return img;
    }
        
    protected void createImage(){
        //метод создает обьект изображение
        ImageIcon iia = new ImageIcon(this.imageAddr);
        img = iia.getImage();  
    }
    
    protected Rectangle getRectangle(int y, int width, int height ){
         return new Rectangle(x, y, width, height);//метод возвращает прямоугольник с заданными параметрами
    }
}
