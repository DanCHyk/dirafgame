
package imageClasses;

import java.awt.Rectangle;


public class ImageGiraf extends ImageCreator {
    private final int startPositionY = 250;//стартовое положение обьекта по оси y
    private final int maxPositionY = 100;//максимальное положение обьекта по оси y
    
    private int y = 250;//положение обьекта по оси y
    private boolean girafUp = false;//переменная определяет, движется ли обьект вверх для "прыжка"

    public ImageGiraf(String imageAddr) {
        x = 20;  
        super.imageAddr = imageAddr;
        getImage();
    }

    public int getStartPositionY() {
        return startPositionY;
    }

    public boolean isGirafUp() {
        return girafUp;
    }

    public void setGirafUp(boolean girafUp) {
        this.girafUp = girafUp;
    }
    
    
    public void setY(int y) {
        this.y -= y;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void girafMove() {
        //метод отвечает за прыжки жирафа. Обьект во время прыжка может переместиться до положения 100 пикселей по оси y, после чего обьект должен возвратиться на стартовое положение
        if ((y == startPositionY || y > maxPositionY) && girafUp) {
            setY(50);

        } else if (y == maxPositionY && girafUp) {
            //girafUp = false;
            setGirafUp(false);
        } else if (!girafUp && y < startPositionY) {
            setY(-50);
        }
    }

    public Rectangle getGirafRect() {
        
        return super.getRectangle(y, 70, 120);
    }
}
