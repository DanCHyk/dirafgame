
package imageClasses;

import imageClasses.ImageCreator;


public class ImageSky extends ImageCreator{
  
  private final int y = 0;

  
    public ImageSky(String imageAddr) {
      super.imageAddr = imageAddr;
      super.x = 0;
      getImage();
    }

    public int getY() {
        return y;
    }
    public int getX() {
        return x;
    }   
}
