
package imageClasses;

import imageClasses.ImageCreator;
import java.awt.Image;
import java.awt.Rectangle;

import java.util.LinkedList;
import java.util.List;



public class ImageTree extends ImageCreator{
    private final int startPositionX = 640;//стартовая позиция появления обьекта дерева на игровом поле
    private final int pixelStep = 10;//шаг для смещения обьекта в пикселях
    public int getX() {
        //получаем положение обьекта на игровом поле со смещением на 10 пикселей
        x-=pixelStep;
        return x;
    }
    
    //коллекция содержит все позиции обьекта дерево
    List<Integer> xPositionTree= new LinkedList<>();
    
    public ImageTree(String imageAddr) {
       x = startPositionX;
      super.imageAddr = imageAddr;
      getImage();
      
    }

    public List<Integer> getxPositionTree() {
       xPositionTree.add(0, 620);
        return xPositionTree;
    }

   
    
   public Rectangle getTreeRect(){
        //метод отдает параметры прямоуголтника, в которые помещено дерево
        return super.getRectangle(335, 20, 70);
    }
    
}
