package girafgame;

import imageClasses.ImageTree;
import imageClasses.ImageSky;
import imageClasses.ImageGiraf;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class GamePanel extends JPanel implements ActionListener, Runnable {
///test 223 dana_test commited only dana_test2 push334
    Timer timer;
    Graphics gr;
    ImageTree it = new ImageTree("treeYellow.png"); //создание картинки "дерево"
    private final int y = 335; // опрежедяет положение элемента по оси y

    boolean inGame = true;//переменная определяет, находиться ли пользователь в игре или игра закончина проигрышем

    List<ImageTree> itree = new ArrayList<>();//коллекция содержит перечень обьектов которые описывают положение обьекта "дерево" на экране в каждый момент времени и другую логику, связанную с обьектовм
    ImageGiraf igiraf = new ImageGiraf("Giraf.png");//переменная содержит обьект "жираф" и его свойства и положение на игровом поле
    ImageSky isky = new ImageSky("sky.png");//переменная содержит обьект "небо" и его свойства и положение на игровом поле

    Thread th = new Thread(this);

    public GamePanel() {
        setBackground(Color.BLACK);

        timer = new Timer(100, this);
        timer.start();

        //поток, который создает деревья
        th.start();
        addKeyListener(new FieldKeyListenerButton());//добавляет слушатель нажатия кнопок 
        setFocusable(true);

    }

    private void paintGround() {
        if (inGame) {
            gr.setColor(Color.yellow);
            gr.drawLine(0, 400, 700, 400);//отрисовка линии которая обозначает поверхность по которой двигаются обьекты игры
            gr.drawImage(isky.getImage(), isky.getX(), isky.getY(), this);//отрисовка неба(тучи и солнце)

            Iterator<ImageTree> iterat = itree.iterator();//отрисовка деревьев
            //перебираем все обьекты деревья, проверяем пересечение с обьектом "жираф",перемещаем их по оси x и удаляем те, которые вышли за предел игрового поля
            while (iterat.hasNext()) {
                ImageTree imageTree = iterat.next();

                if (imageTree.getTreeRect().intersects(igiraf.getGirafRect())) {
                    inGame = false; //проверяем, пересеклись ли обьекты "жираф" и "деревья". если пересеклис - отображаем панель "game over"
                }

                if (imageTree.getX() < -40) {
                    iterat.remove(); //если дерево вышло за пределы игрового поля - оно удаляется из списка деревьев, который нужно отобразить
                } else {
                    gr.drawImage(imageTree.getImage(), imageTree.getX(), y, this); //отрисовываем обьект "деревья" со смещением на 10 пикселей
                }
            }
            igiraf.girafMove();//прыжки жирафа
        } else {

            gr.drawImage(new ImageIcon("game over.png").getImage(), 100, 100, this);//отображения панели "game over"
        }
    }

    private void paintGiraf() {
        gr.drawImage(igiraf.getImage(), igiraf.getX(), igiraf.getY(), this);  //отображение текущего положения жирафа в пространстве
    }

    @Override
    protected void paintComponent(Graphics grphcs) {//метод запускается при перерисовке компонента
        super.paintComponent(grphcs);
        gr = grphcs;
        paintGround(); //отрисовка линнии "земли"(поверхности) и всего 
        paintGiraf();//отрисовка жирафа

    }

    @Override
    public void actionPerformed(ActionEvent ae) { //запкускается при каждом вызове таймера 
        repaint();

    }

    @Override
    public void run() {
        //в отдельном потоке создаем сет деревьев/ Логика создания основана на задержке потока добавления обьекта "дерево" в список деревьев на рандомное время
        while (true) {
            Random rand = new Random();
            int randInt = rand.nextInt(3000);
            while (randInt < 1000) {
                randInt = rand.nextInt(3000);
            }
            try {
                Thread.sleep(randInt);
            } catch (InterruptedException ex) {
                System.out.println("Error while create object tree");
            }
            itree.add(new ImageTree("treeYellow.png"));

        }
    }

    class FieldKeyListenerButton extends KeyAdapter {

        //внутренный класс, который отвечает за прослушивание нажатия кнопок вверх и enter
        @Override
        public void keyPressed(KeyEvent e) {
            super.keyPressed(e);
            int key = e.getKeyCode();

            if (key == KeyEvent.VK_UP) {

                if (igiraf.getY() == igiraf.getStartPositionY()) {
                    igiraf.setGirafUp(true);//при нажатии клавиши вверх обьект должен перемещатся по оси у вверх
                }

            }
            if (key == KeyEvent.VK_ENTER) {
                itree = new ArrayList<>();
                inGame = true; //при нажатии enter игра запускается заново
            }
        }
    }
}
